package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private final String DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];;
    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        EditText pw = view.findViewById((R.id.tvLogin));
        Spinner spSa = view.findViewById(R.id.spSalle);
        // TODO Q1
        this.poste = "";
        this.salle = this.DISTANCIEL;
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        //Spinner sp = ArrayAdapter.createFromResource(getContext(), R.array.list_salles, spSa);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            model.setUsername(pw.getText().toString());
            Toast.makeText(getContext(), pw.getText().toString(), Toast.LENGTH_SHORT).show();
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);

        });

        // TODO Q5.b
        // TODO Q9
    }

    // TODO Q5.a
    // TODO Q9
}